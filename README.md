# catalog-creation-pipeline-domain

Catalog creation pipeline system 에서 사용하는 Domain Layer 입니다.

JPA 기반 domain과 [requery](https://github.com/requery/requery) 기반 domain 을 제공합니다.