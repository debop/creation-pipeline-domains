package com.coupang.catalog.creation.domains.model

import com.coupang.catalog.creation.domains.core.EntityType
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
class CreationEntity(entityType: EntityType = EntityType.DEFAULT) : AbstractEntity(entityType) {

  @get:Id
  @get:GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Long = 0L

  var externalTxId: String? = null
  // TODO: avro 참조
  // var msgType: MessageType? = null
  var requestedProductId: Long? = null
  var seq: Long? = null

  // TODO: 참고 추가
  // var requestedFrom: RequestedFrom? = null

  var bizTask: String? = null
  var revision: String? = null
  var content: String? = null
  var topicMeta: String? = null

}