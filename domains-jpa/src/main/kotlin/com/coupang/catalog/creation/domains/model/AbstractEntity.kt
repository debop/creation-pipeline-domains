package com.coupang.catalog.creation.domains.model

import com.coupang.catalog.creation.domains.core.EntityType
import com.coupang.catalog.creation.domains.core.Traceable
import java.util.*
import javax.persistence.*

@MappedSuperclass
@Access(AccessType.FIELD)
abstract class AbstractEntity(val entityType: EntityType = EntityType.DEFAULT) : Traceable {

  var cas: Long = 0L
  var expiry: Int = 0

  override var createdAt: Date? = null
  override var updatedAt: Date? = null

  init {
    createdAt = Date()
    updatedAt = Date()
  }

  val typeName: String get() = javaClass.name

  // TODO: kotlinx-jpa-entity 에 있는 JpaEntity 를 상속해야 한다. 
}