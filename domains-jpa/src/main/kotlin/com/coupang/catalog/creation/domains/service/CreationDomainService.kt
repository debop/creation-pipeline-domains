///*
// * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *       http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.coupang.catalog.creation.domains.service
//
//import mu.KLogging
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.stereotype.Service
//
///**
// * CreationDomainService
// *
// * @author sunghyouk.bae@gmail.com
// * @since  17. 11. 12
// */
//@Service
//class CreationDomainService @Autowired constructor(val appConfig: ApplicationConfig,
//                                                   val kafkaProducerMapper: KafkaProducerMapper) {
//
//  companion object : KLogging() {
//    /** LeadTime for BEGIN / END (default: 1 days) */
//    const val LEADTIME_EXPIRATION = 1 * 24 * 60 * 60 // 1 day in milliseconds
//
//    /** LeadTime for DONE (default: 7 days) */
//    const val LEADTIME_EXPIRATION_DONE = 7 * 24 * 60 * 60  // 7 days in milliseconds
//
//    const val VIEW_TIMEOUT_SECOND = 30L
//
//    const val LOG_HEADER = "[CREATION-PIPELINE]"
//  }
//
//
//  fun persistDeadLetter(data: ByteArray?) {
//    if (data == null || data.isEmpty()) {
//      logger.error { "$LOG_HEADER message is null or size is zero." }
//      return
//    }
//
//    try {
//      persistDeadLetter(deserialize(data, CatalogMsgEnvelop::class.java))
//    } catch (e: Exception) {
//      logger.error(e) { "$LOG_HEADER Fail to deserialize message." }
//    }
//  }
//
//  fun persistDeadLetter(envelop: CatalogMsgEnvelop) {
//
//  }
//}