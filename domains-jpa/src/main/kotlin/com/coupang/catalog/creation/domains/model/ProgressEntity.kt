package com.coupang.catalog.creation.domains.model

import com.coupang.catalog.creation.domains.core.EntityType
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
class ProgressEntity(entityType: EntityType = EntityType.DEFAULT) : AbstractEntity(entityType) {

  @get:Id
  @get:GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Long = 0L

  var externalTxId: String? = null
  var revision: String? = null
  var bizTask: String? = null

  // TODO: reference 추가
  // var msgType:MessageType

  var msgPriority: String? = null
  var sourceTopicMeta: String? = null
  var targetTopicMeta: String? = null
  var requestedProductId: Long = 0L

}