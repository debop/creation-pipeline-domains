package com.coupang.catalog.creation.domains.model

import com.coupang.catalog.creation.domains.core.EntityType
import javax.persistence.*

/**
 * DeadLetterEntity
 *
 * @author sunghyouk.bae@gmail.com
 * @since  17. 11. 12
 */
@Entity
class DeadLetterEntity(entityType: EntityType = EntityType.DEFAULT) : AbstractEntity(entityType) {

  @get:Id
  @get:GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Long = 0L

  var status: String = "requested"


  @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
  var creation: CreationEntity? = null

}