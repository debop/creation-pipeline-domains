/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coupang.catalog.creation.domains.entities

import com.coupang.catalog.creation.doamins.entities.Creation
import com.coupang.catalog.creation.domains.AbstractDomainTest
import org.junit.Test
import kotlin.test.*

/**
 * CreationTest
 *
 * @author sunghyouk.bae@gmail.com
 * @since  17. 11. 12
 */
class CreationTest : AbstractDomainTest() {

  @Test fun `create Creation entity`() {
    val creation = Creation().apply {
      seq = 1
    }

    data.insert(creation)

    val found = data.findByKey(Creation::class.java, creation.id)
    assertNotNull(found)

    assertEquals(1, found!!.seq)
    assertNotNull(found.createdAt)
    assertNull(found.updatedAt)
  }

  @Test fun `listener functions`() {
    val creation = Creation().apply { seq = 1 }

    data.upsert(creation)

    val found = data.findByKey(Creation::class.java, creation.id)
    assertNotNull(found)

    assertEquals(1, found!!.seq)
    assertNotNull(found.createdAt)
    assertNull(found.updatedAt)

    data.update(found)
    assertNotNull(found.updatedAt)
  }
}