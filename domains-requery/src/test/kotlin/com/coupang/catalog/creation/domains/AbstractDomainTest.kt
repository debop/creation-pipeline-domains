/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coupang.catalog.creation.domains


import com.coupang.catalog.creation.doamins.entities.Models
import com.coupang.catalog.creation.domains.utils.DatabaseType
import io.requery.Persistable
import io.requery.cache.EmptyEntityCache
import io.requery.sql.*
import org.junit.After
import org.junit.Before

/**
 * AbstractDomainTest
 *
 * @author sunghyouk.bae@gmail.com
 * @since  17. 11. 12
 */
abstract class AbstractDomainTest {

  lateinit var data: EntityDataStore<Persistable>

  @Before fun setup() {
    val dataSource = DatabaseType.H2.datasourceFactory()
    val model = Models.DEFAULT

    val configuration = ConfigurationBuilder(dataSource, model)
        .useDefaultLogging()
        .setEntityCache(EmptyEntityCache())
        .build()

    val tables = SchemaModifier(configuration)
    tables.createTables(TableCreationMode.DROP_CREATE)
    data = EntityDataStore(configuration)
  }

  @After fun cleanup() {
    data.close()
  }
}