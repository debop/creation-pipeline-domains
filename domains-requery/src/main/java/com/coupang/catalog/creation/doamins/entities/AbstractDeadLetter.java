/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coupang.catalog.creation.doamins.entities;

import io.requery.*;

/**
 * AbstractDeadLetter
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 13
 */
@Entity(copyable = true)
public abstract class AbstractDeadLetter extends AbstractTraceableObject {

    @Key
    @Generated
    Long id;

    String status = "requested";

    @ManyToOne(cascade = CascadeAction.NONE)
    Creation creation;

//    Integer expiry = 0;
//    LocalDateTime createdAt;
//    LocalDateTime updatedAt;
//
//    @PreInsert
//    public void onPreInsert() {
//        createdAt = LocalDateTime.now();
//    }
//
//    @PreUpdate
//    public void OnPreUpdate() {
//        updatedAt = LocalDateTime.now();
//    }


    private static final long serialVersionUID = -5710611423526186075L;
}
