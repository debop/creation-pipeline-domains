/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coupang.catalog.creation.doamins.entities;

import io.requery.Persistable;
import io.requery.PreInsert;
import io.requery.PreUpdate;
import io.requery.Superclass;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * com.coupang.catalog.creation.doamins.entities.AbstractTraceableObject
 *
 * @author debop
 * @since 17. 11. 13
 */
@Superclass
public abstract class AbstractTraceableObject implements Serializable, Persistable {

    protected Integer expiry = 0;

    protected LocalDateTime createdAt;

    protected LocalDateTime updatedAt;


    @PreInsert
    public void onPreInsert() {
        createdAt = LocalDateTime.now().withNano(0);
    }

    @PreUpdate
    public void onPreUpdate() {
        updatedAt = LocalDateTime.now().withNano(0);
    }


    private static final long serialVersionUID = 2029072049605401459L;
}
