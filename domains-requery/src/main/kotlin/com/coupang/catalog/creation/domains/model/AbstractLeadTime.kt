///*
// * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *       http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.coupang.catalog.creation.domains.model
//
//import io.requery.*
//import java.io.Serializable
//import java.time.LocalDateTime
//
///**
// * AbstractLeadTime
// *
// * @author sunghyouk.bae@gmail.com
// * @since  17. 11. 12
// */
//@Entity
//abstract class AbstractLeadTime : Serializable {
//
//  @get:Key
//  @get:Generated
//  open var id: Long = 0L
//
//  @get:ManyToOne(cascade = arrayOf(CascadeAction.NONE))
//  // @get:Index("idx_leadtime_creation")
////  @get:ForeignKey(delete = ReferentialAction.NO_ACTION,update=ReferentialAction.RESTRICT)
//  open var creation: Creation? = null
//
//  open var internalTxId: String? = null
//  open var taskJob: String? = null
//  open var step: String? = null
//
//  open var expiry: Int = 0
//
//  open var createdAt: LocalDateTime? = null
//  open var updatedAt: LocalDateTime? = null
//
//  @PreInsert
//  open fun onPreInsert() {
//    createdAt = LocalDateTime.now().withNano(0)
//  }
//
//  @PreUpdate
//  open fun onPreUpdate() {
//    updatedAt = LocalDateTime.now().withNano(0)
//  }
//}