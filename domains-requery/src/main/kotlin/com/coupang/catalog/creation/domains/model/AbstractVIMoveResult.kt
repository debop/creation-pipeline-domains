///*
// * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *       http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.coupang.catalog.creation.domains.model
//
//import io.requery.*
//import java.io.Serializable
//import java.time.LocalDateTime
//
///**
// * AbstractVIMoveResult
// *
// * @author sunghyouk.bae@gmail.com
// * @since  17. 11. 12
// */
//@Entity
//abstract class AbstractVIMoveResult : Serializable {
//
//  @get:Key
//  @get:Generated
//  open var id: Long = 0L
//
//  open var internalTxId: String? = null
//  open var vendorItemId: Long = 0L
//  open var requestedItemId: Long = 0L
//  open var matchingType: String? = null
//
//  @get:ManyToOne(cascade = arrayOf(CascadeAction.SAVE))
////  @get:Index("idx_vimoveresult_creation")
////  @get:ForeignKey(delete = ReferentialAction.SET_NULL, update = ReferentialAction.NO_ACTION)
//  open var creation: Creation? = null
//
//  open var expiry: Int = 0
//
//  open var createdAt: LocalDateTime? = null
//  open var updatedAt: LocalDateTime? = null
//
//  @PreInsert
//  open fun onPreInsert() {
//    createdAt = LocalDateTime.now().withNano(0)
//  }
//
//  @PreUpdate
//  open fun onPreUpdate() {
//    updatedAt = LocalDateTime.now().withNano(0)
//  }
//}