/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coupang.catalog.creation.domains.utils

import io.requery.sql.Platform
import org.h2.jdbcx.JdbcDataSource
import javax.sql.CommonDataSource
import kotlin.reflect.KClass

/**
 * DatabaseType
 *
 * @author sunghyouk.bae@gmail.com
 * @since  17. 11. 12
 */
enum class DatabaseType(val platformClass: KClass<out Platform>,
                        val dataSourceClass: KClass<out CommonDataSource>,
                        val dataSourceClassName: String = "",
                        val datasourceFactory: () -> CommonDataSource) {

  // POSTGRES(PostgresSQL::class, PGSimpleDataSource::class)

  H2(io.requery.sql.platform.H2::class, JdbcDataSource::class, "", {
    JdbcDataSource().apply {
      setUrl("jdbc:h2:~/testh2")
      user = "sa"
      password = "sa"
    }
  })

}